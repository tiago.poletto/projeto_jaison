<?php
include('db.php');

$query = sprintf("SELECT id, participante, descricao FROM avaliacao WHERE status = 0");
// executa a query
$dados = mysqli_query($con, $query) or die(mysqli_error());
// transforma os dados em um array
$linha = mysqli_fetch_assoc($dados);
// calcula quantos dados retornaram
$total = mysqli_num_rows($dados);

    // se o número de resultados for maior que zero, mostra os dados
    if($total > 0) {
        // inicia o loop que vai mostrar todos os dados
        do {
            $idAvaliacao = $linha['id'];
?>
<tr>
    <td><?=$linha["participante"]?></td>
    <td><?=$linha["descricao"]?></td>
    <td>
    	<form action="../php/avaliar.php" method="POST">
          <div class="form-group">
	            <div class="form-label-group">
	              	<input type="number" id="nota" class="form-control" placeholder="Nota" name="nota">
	              	<label for="nota">Nota</label>
	            </div>
	            <div class="form-label-group">
			        <input type="hidden" id="idParticipante" value="<?=$linha["id"]?>" name="idParticipante">
	              	<!--<label for="idParticipante">Confirmar ID</label>-->
	            </div>
          </div>
          <button type="submit" class="btn btn-success">Avaliar</button>
        </form>
    </td>
</tr>
<?php
        // finaliza o loop que vai mostrar os dados
        }while($linha = mysqli_fetch_assoc($dados));
    // fim do if 
    }

?>