DESCRIÇÃO (JAISON)

Deseja-se desenvolver um sistema de avaliação de desempenho e premiação para os melhores participantes, onde seja possível definir indicadores de desempenho, metas para as pessoas que serão avaliadas, pessoas envolvidas nas avaliações, os métodos de avaliação que deveram ser dinâmicos para os diferentes participantes.
Os indicadores de avaliação deveram ser quantitativos e qualitativos.
Os participantes envolvidos serão recompensados de acordo com suas metas e o atingimentos destas.
Assim um sistema que tenha uma equidade para que os participantes possam competir de forma justa


MODELAGEM Nº 1 (GREICI)

O software deve cadastrar participantes.
O software deve avaliar o desempenho dos participantes.
O software deve premiar os melhores participantes, os quais atingirem suas metas e tiverem os melhores resultados.
O software deve definir os indicadores de desempenho quantitativos e qualitativos.
O software deve definir as metas do participante.
O software deve definir métodos de avaliação.


Participante:
- id
- nome
- e-mail
- senha

Indicador
- nome
- peso

Avaliação
- indicador
- participante
- metodologia
- metas

Metologia
- regras



MODELAGEM Nº 2 (APÓS PROFESSOR VERIFICAR PEDIU PARA SIMPLIFICAR) (GREICI)


O software deve cadastrar avaliador.
O software deve cadastrar avaliação.
O software deve gerar ranking das avaliações.

Avaliador
- id
- nome
- e-mail
- senha

Avaliação
- nome particiante
- descrição
- nota

Ranking
- relatório avaliação


PROTOTIPO (TIAGO)
